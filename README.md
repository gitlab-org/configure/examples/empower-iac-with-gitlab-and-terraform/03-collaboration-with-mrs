# 03 Collaboration with Merge Requests

This project illustrates a very basic Terraform setup
which uses the [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
and the [GitLab Terraform CI/CD template](https://docs.gitlab.com/ee/user/infrastructure/iac/#latest-terraform-template)
to illustrate how we can collaborate with the [Terraform Merge Request Integration](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html).

See the open Merge Request: [here](https://gitlab.com/gitlab-org/configure/examples/empower-iac-with-gitlab-and-terraform/03-collaboration-with-mrs/-/merge_requests/1).